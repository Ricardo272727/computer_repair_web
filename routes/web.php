<?php

use App\Http\Controllers\AppointmentController;
use App\Http\Controllers\CommentController;
use App\Http\Controllers\ContactController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\RepairController;
use App\Models\Comment;
use App\Models\Service;
use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;
use Symfony\Component\HttpFoundation\Request;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function (Request $request) {
    $message = $request->getSession()->get('message');
    $services = Service::query()->get();
    $comments = DB::table('comments')
      ->join('users', 'comments.client_id', '=', 'users.id')
      ->select(
        'comments.id',        
        'users.name',
        'comments.content',
      )->get();

    return Inertia::render('Welcome', [
        'canLogin' => Route::has('login'),
        'canRegister' => Route::has('register'),
        'laravelVersion' => Application::VERSION,
        'phpVersion' => PHP_VERSION,
        'services' => $services,
        'comments' => $comments,
        'message' => $message, 
    ]);
})->name('welcome');
Route::get('/about', function(){
  return Inertia::render('About');
})->name('about');
Route::get('/services', function(){
  $services = Service::query()->get();
  return Inertia::render('Services', [
    'services' => $services,
  ]);
})->name('services');

Route::get('/dashboard', function () {
    return Inertia::render('Dashboard');
})->middleware(['auth', 'verified'])->name('dashboard');

Route::controller(RepairController::class)->group(function(){
  Route::get('client-repairs/{id}', 'repairs_by_client')->name('client-repairs.index');
  Route::post('cancel-repair/{id}', 'cancel_repair')->name('repairs.cancel');
  Route::post('finish-repair/{id}', 'finish_repair')->name('repairs.finish');
})->middleware(['auth', 'verified']);
Route::resource('repairs', RepairController::class)->middleware(['auth', 'verified']);

Route::controller(CommentController::class)->group(function(){
  Route::get('new-comment', 'create')->name('comments.create');
  Route::post('store-comment', 'store')->name('comments.store');
})->middleware(['auth', 'verified']);

Route::controller(ContactController::class)->group(function(){
  Route::post('contact', 'send')->name('contacts.send');
  Route::get('contacts', 'index')->name('contacts.index');
})->middleware(['auth', 'verified']);




Route::controller(AppointmentController::class)->group(function () {
    Route::get('appointments/edit/all', 'edit')
        ->name('appointments.edit_all')
        ->middleware(['auth', 'verified']);
    ;
    Route::put('appointments/update/all', 'update')
        ->name('appointments.update_all')
        ->middleware(['auth', 'verified']);
    ;
});

Route::resource('appointments', AppointmentController::class)->middleware(['auth', 'verified']);

Route::middleware('auth')->group(function () {
    Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
    Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
    Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');
});

require __DIR__ . '/auth.php';
