import { useCallback, useState } from "react";
import { useForm } from "@inertiajs/react";

export const useFinishRepair = () => {
    const form = useForm({});
    const [repair, setRepair] = useState({});
    const [showModal, setShowModal] = useState(false);

    const onClick = (repair) => {
        setRepair(repair);
        setShowModal(true);
    };

    const onAccept = useCallback(() => {
        setShowModal(false);
        console.log("Finish repair...", repair);
        form.post("/finish-repair/" + repair.id);
        setRepair({});
    }, [repair]);

    return {
        onAccept,
        onClick,
        onCloseModal: () => {
            setShowModal(false);
            setRepair({});
        },
        show: showModal,
        repair: repair,
    };
};
