import { useCallback, useState } from "react";
import { useForm } from "@inertiajs/react";

export const useCancelRepair = () => {
    const form = useForm({});
    const [repair, setRepair] = useState({});
    const [showModal, setShowModal] = useState(false);

    const onClickCancel = (repair) => {
        setRepair(repair);
        setShowModal(true);
        console.log({ repair, faack: true });
    };

    const onAcceptCancel = useCallback(() => {
        setShowModal(false);
        console.log("Deleting repair...", repair);
        form.post("/cancel-repair/" + repair.id);
    }, [repair]);

    return {
        onAcceptCancel,
        onClickCancel,
        onCloseModal: () => {
            setShowModal(false);
            setRepair({});
        },
        show: showModal,
        repair: repair,
    };
};
