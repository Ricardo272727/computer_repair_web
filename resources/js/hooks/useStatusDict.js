export const useStatusDict = () => ({
    pending: "Pendiente",
    finished: "Terminada",
    cancelled: "Cancelada",
});
