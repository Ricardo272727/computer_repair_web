import AuthenticatedLayout from "@/Layouts/AuthenticatedLayout";
import ServiceRepairRow from "@/Components/ServiceRepairRow";
import { Head, useForm, usePage } from "@inertiajs/react";
import { useEffect, useState } from "react";
import { ErrorAlert } from "@/Components/ErrorAlert";

export default function CreateComment(props) {
    const serverData = usePage().props;
    const form = useForm({
        content: "",
    });
    const [error, setError] = useState("");
    const onChange = (e) => {
        form.setData({ content: e.target.value });
    };
    const onSubmit = (e) => {
        e.preventDefault();
        if(!form.data.content){
          setError('Este campo es requerido');
          return false;
        }
        console.log({ form });
        form.post(route('comments.store'));
    };

    return (
        <AuthenticatedLayout
            auth={props.auth}
            errors={props.errors}
            header={
                <h2 className="font-semibold text-xl text-gray-800 leading-tight">
                    Deja un comentario
                </h2>
            }
        >
            <Head title="Cliente" />

            <div className="container mx-auto px-2 pt-6">
                <form
                    onSubmit={onSubmit}
                    className="w-full max-w-screen-lg mx-auto"
                >
                    <textarea
                        name="content"
                        className="block p-2.5 w-full text-md text-gray-500 bg-white rounded min-h-[10rem] mb-2"
                        placeholder="Muy buen servicio!"
                        value={form.data.content}
                        onChange={onChange}
                    ></textarea>
                    {error && <ErrorAlert message={error} />}
                    <div className="container flex justify-end py-5 px-2">
                        <button
                            type="submit"
                            className="bg-gray-800 hover:bg-gray-500 text-white font-bold py-2 px-4 rounded cursor-pointer"
                        >
                            Enviar comentario
                        </button>
                    </div>
                </form>
            </div>
        </AuthenticatedLayout>
    );
}
