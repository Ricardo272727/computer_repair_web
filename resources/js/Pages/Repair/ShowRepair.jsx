import { useStatusDict } from "@/hooks/useStatusDict";
import AuthenticatedLayout from "@/Layouts/AuthenticatedLayout";
import { Head, useForm, usePage } from "@inertiajs/react";
import moment from "moment";

export default function ShowRepair(props) {
    const serverData = usePage().props;
    const repair = serverData.repair || {};
    const appointments = serverData.appointments || [];
    const user = serverData.user || {};
    const services = serverData.services || [];
    const statusDict = useStatusDict();
    console.log({ repair, appointments, user, services });

    return (
        <AuthenticatedLayout
            auth={props.auth}
            errors={props.errors}
            header={
                <h2 className="font-semibold text-xl text-gray-800 leading-tight">
                    Información de reparación #{repair.id}
                </h2>
            }
        >
            <Head title="Detalle reparación" />

            <div className="md:container md:mx-auto px-2 sm:px-6 lg:px-8 text-center my-8">
                <h1 className="text-4xl">Información general</h1>
            </div>

            <div className="container mx-auto px-2 max-w-screen-2xl overflow-x-scroll text-center text-center">
                <table className="min-w-full">
                    <thead className="bg-white border-b">
                        <tr>
                            <th
                                scope="col"
                                className="text-sm font-medium text-gray-900 px-6 py-4"
                            >
                                Status
                            </th>
                            <th
                                scope="col"
                                className="text-sm font-medium text-gray-900 px-6 py-4"
                            >
                                Cliente
                            </th>
                            <th
                                scope="col"
                                className="text-sm font-medium text-gray-900 px-6 py-4"
                            >
                                Costo total (MXN)
                            </th>
                            <th
                                scope="col"
                                className="text-sm font-medium text-gray-900 px-6 py-4"
                            >
                                Tiempo total (Horas)
                            </th>
                            <th
                                scope="col"
                                className="text-sm font-medium text-gray-900 px-6 py-4"
                            >
                                Fecha de creación
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr className="bg-gray-100 border-b">
                            <td className="px-6 py-4 whitespace-nowrap text-sm font-medium text-gray-900">
                                {statusDict[repair.status.trim()]}
                            </td>
                            <td className="px-6 py-4 whitespace-nowrap text-sm font-medium text-gray-900">
                                {user.name}
                            </td>
                            <td className="px-6 py-4 whitespace-nowrap text-sm font-medium text-gray-900">
                                {repair.total_cost}
                            </td>
                            <td className="px-6 py-4 whitespace-nowrap text-sm font-medium text-gray-900">
                                {repair.total_time}
                            </td>
                            <td className="px-6 py-4 whitespace-nowrap text-sm font-medium text-gray-900">
                                {moment(repair.created_at).format(
                                    "DD-MM-YYYY HH:MM"
                                )}
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>

            <div className="md:container md:mx-auto px-2 sm:px-6 lg:px-8 text-center my-8">
                <h1 className="text-4xl">Información del cliente</h1>
            </div>

            <div className="container mx-auto px-2 max-w-screen-2xl overflow-x-scroll text-center">
                <table className="min-w-full">
                    <thead className="bg-white border-b">
                        <tr>
                            <th
                                scope="col"
                                className="text-sm font-medium text-gray-900 px-6 py-4"
                            >
                                ID cliente
                            </th>
                            <th
                                scope="col"
                                className="text-sm font-medium text-gray-900 px-6 py-4"
                            >
                                Correo electrónico
                            </th>
                            <th
                                scope="col"
                                className="text-sm font-medium text-gray-900 px-6 py-4"
                            >
                                Celular
                            </th>
                            <th
                                scope="col"
                                className="text-sm font-medium text-gray-900 px-6 py-4"
                            >
                                Ciudad
                            </th>
                            <th
                                scope="col"
                                className="text-sm font-medium text-gray-900 px-6 py-4"
                            >
                                Municipio
                            </th>
                            <th
                                scope="col"
                                className="text-sm font-medium text-gray-900 px-6 py-4"
                            >
                                Calle y número
                            </th>
                            <th
                                scope="col"
                                className="text-sm font-medium text-gray-900 px-6 py-4"
                            >
                                Código postal
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr className="bg-gray-100 border-b">
                            <td className="px-6 py-4 whitespace-nowrap text-sm font-medium text-gray-900">
                                {user.id}
                            </td>
                            <td className="px-6 py-4 whitespace-nowrap text-sm font-medium text-gray-900">
                                {user.email}
                            </td>
                            <td className="px-6 py-4 whitespace-nowrap text-sm font-medium text-gray-900">
                                {user.phone_number}
                            </td>
                            <td className="px-6 py-4 whitespace-nowrap text-sm font-medium text-gray-900">
                                {user.city}
                            </td>
                            <td className="px-6 py-4 whitespace-nowrap text-sm font-medium text-gray-900">
                                {user.municiple}
                            </td>
                            <td className="px-6 py-4 whitespace-nowrap text-sm font-medium text-gray-900">
                                {user.street + " " + user.street_number}
                            </td>
                            <td className="px-6 py-4 whitespace-nowrap text-sm font-medium text-gray-900">
                                {user.postal_code}
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>

            <div className="md:container md:mx-auto px-2 sm:px-6 lg:px-8 text-center my-8">
                <h1 className="text-4xl">Servicios</h1>
            </div>

            <div className="container mx-auto px-2 max-w-screen-2xl overflow-x-scroll text-center text-center">
                <table className="min-w-full">
                    <thead className="bg-white border-b">
                        <tr>
                            <th
                                scope="col"
                                className="text-sm font-medium text-gray-900 px-6 py-4"
                            >
                                ID
                            </th>
                            <th
                                scope="col"
                                className="text-sm font-medium text-gray-900 px-6 py-4"
                            >
                                Nombre
                            </th>
                            <th
                                scope="col"
                                className="text-sm font-medium text-gray-900 px-6 py-4"
                            >
                                Precio (MXN)
                            </th>
                            <th
                                scope="col"
                                className="text-sm font-medium text-gray-900 px-6 py-4"
                            >
                                Tiempo total (Horas)
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        {services.map((service) => (
                            <tr key={service.id} className="bg-gray-100 border-b">
                                <td className="px-6 py-4 whitespace-nowrap text-sm font-medium text-gray-900">
                                    {service.id}
                                </td>
                                <td className="px-6 py-4 whitespace-nowrap text-sm font-medium text-gray-900">
                                    {service.name}
                                </td>
                                <td className="px-6 py-4 whitespace-nowrap text-sm font-medium text-gray-900">
                                    {service.price}
                                </td>
                                <td className="px-6 py-4 whitespace-nowrap text-sm font-medium text-gray-900">
                                    {service.duration}
                                </td>
                            </tr>
                        ))}
                    </tbody>
                </table>
            </div>

            <div className="md:container md:mx-auto px-2 sm:px-6 lg:px-8 text-center my-8">
                <h1 className="text-4xl">Citas</h1>
            </div>

            <div className="container mx-auto px-2 max-w-screen-2xl overflow-x-scroll">
                <table className="min-w-full">
                    <thead className="bg-white border-b">
                        <tr>
                            <th
                                scope="col"
                                className="text-sm font-medium text-gray-900 px-6 py-4"
                            >
                                ID
                            </th>
                            <th
                                scope="col"
                                className="text-sm font-medium text-gray-900 px-6 py-4"
                            >
                                Fecha
                            </th>
                            <th
                                scope="col"
                                className="text-sm font-medium text-gray-900 px-6 py-4"
                            >
                                Hora inicio
                            </th>
                            <th
                                scope="col"
                                className="text-sm font-medium text-gray-900 px-6 py-4"
                            >
                                Hora fin
                            </th>
                        </tr>
                    </thead>
                    <tbody className="text-center">
                        {appointments.map((item, index) => (
                            <tr key={index} className="bg-gray-100 border-b">
                                <td className="px-6 py-4 whitespace-nowrap text-sm font-medium text-gray-900">
                                    {item.id}
                                </td>
                                <td className="px-6 py-4 whitespace-nowrap text-sm font-medium text-gray-900">
                                    {moment(item.start_datetime).format(
                                        "DD/MM/YYYY"
                                    )}
                                </td>
                                <td className="text-sm text-gray-900 font-light px-6 py-4 whitespace-nowrap">
                                    {moment(item.start_datetime).format(
                                        "HH:mm"
                                    )}
                                </td>
                                <td className="text-sm text-gray-900 font-light px-6 py-4 whitespace-nowrap">
                                    {moment(item.end_datetime).format("HH:mm")}
                                </td>
                            </tr>
                        ))}
                    </tbody>
                </table>
            </div>
        </AuthenticatedLayout>
    );
}
