import AuthenticatedLayout from "@/Layouts/AuthenticatedLayout";
import ServiceRepairRow from "@/Components/ServiceRepairRow";
import { Head, useForm, usePage } from "@inertiajs/react";
import { useEffect, useState } from "react";
import { ErrorAlert } from "@/Components/ErrorAlert";

const defaultService = {
    id: -1,
    name: "Selecciona un servicio",
    price: 0,
    duration: 0,
};

const mapServices = (services = []) =>
    services.map((service) => ({
        ...service,
        duration: Number(service.duration),
        price: Number(service.price),
    }));

export default function CreateRepair(props) {
    const serverData = usePage().props;
    const isEdit =
        Boolean(serverData.repair) &&
        Boolean(serverData.current_repair_services);
    const repair = serverData.repair || {};
    const currentRepairServices = isEdit
        ? mapServices(serverData.current_repair_services)
        : [];
    const [error, setError] = useState("");
    const { data, setData, post, put } = useForm({
        totalTime: 0,
        totalCost: 0,
        services: [],
    });
    const [availableServices, setAvailableServices] = useState([
        { ...defaultService },
        ...mapServices(serverData.services),
    ]);
    const initialServicesState = isEdit
        ? currentRepairServices
        : [{ ...defaultService }];
    const [services, setServices] = useState(initialServicesState);

    const addService = () =>
        setServices((services) => [...services, { ...defaultService }]);

    console.log({ serverData });

    const onChangeService = (index, e) => {
        const newId = Number(e.target.value);
        const newService = availableServices.find((s) => s.id === newId);
        if (newService) {
            const newServices = services.slice();
            newServices[index] = { ...newService };
            setServices(newServices);
        }
    };

    const onDeleteService = (index, e) => {
        console.log({ delete: index });
        const newServices = services.slice();
        newServices.splice(index, 1);
        setServices(newServices);
    };

    const validate = () => {
        if (services.length === 0 || services.find((s) => s.id === -1)) {
            setError("Por favor selecciona todos los servicios completos");
            return false;
        }
        setError("");
        return true;
    };

    const onSubmit = (e) => {
        e.preventDefault();
        if (!validate()) {
            return;
        }
        const newData = {
            ...data,
            services: services.map((s) => s.id),
        };
        setData(newData);
        if (isEdit) {
            put(route("repairs.update", repair.id));
        } else {
            post(route("repairs.store"));
        }
    };

    useEffect(() => {
        const cost = services.reduce((c, s) => c + s.price, 0);
        const time = services.reduce((c, s) => c + s.duration, 0);
        setData({
            totalCost: cost,
            totalTime: time,
            services: services.map((s) => s.id),
        });
    }, [services]);

    return (
        <AuthenticatedLayout
            auth={props.auth}
            errors={props.errors}
            header={
                <h2 className="font-semibold text-xl text-gray-800 leading-tight">
                    {isEdit
                        ? "Editando reparación #" + repair.id
                        : "Nueva reparación"}
                </h2>
            }
        >
            <Head title="Cliente" />

            <div className="md:container md:mx-auto px-2">
                <form
                    onSubmit={onSubmit}
                    className="w-full max-w-lg mx-auto mt-10 md:min-w-[55%] "
                >
                    {services.map((service, index) => (
                        <ServiceRepairRow
                            key={index}
                            id={service.id}
                            cost={service.price}
                            time={service.duration}
                            availableServices={availableServices}
                            index={index}
                            onChangeService={onChangeService}
                            onDeleteService={onDeleteService}
                        />
                    ))}
                    <div className="container flex justify-center py-5 px-2">
                        <button
                            onClick={addService}
                            type="button"
                            className="bg-green-800 hover:bg-green-500 text-white font-bold py-2 px-4 rounded cursor-pointer"
                        >
                            Agregar servicio +
                        </button>
                    </div>
                    <div className="container flex justify-center py-5 px-2">
                        <div className="w-full md:w-1/2 px-2 mb-6 md:mb-0">
                            <label
                                className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2"
                                htmlFor="grid-city"
                            >
                                Costo total
                            </label>
                            <input
                                className="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500"
                                id="grid-city"
                                type="text"
                                placeholder="Costo total"
                                value={data.totalCost}
                                disabled
                            />
                        </div>
                        <div className="w-full md:w-1/2 px-2 mb-6 md:mb-0">
                            <label
                                className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2"
                                htmlFor="grid-city"
                            >
                                Tiempo total (horas)
                            </label>
                            <input
                                className="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500"
                                id="grid-city"
                                type="text"
                                placeholder="Horas totales"
                                value={data.totalTime}
                                disabled
                            />
                        </div>
                    </div>
                    {error && <ErrorAlert message={error} />}
                    <div className="container flex justify-end py-5 px-2">
                        <button
                            type="submit"
                            className="bg-gray-800 hover:bg-gray-500 text-white font-bold py-2 px-4 rounded cursor-pointer"
                        >
                            {isEdit
                                ? "Guardar y revisar cita(s)"
                                : "Guardar y agendar cita(s)"}
                        </button>
                    </div>
                </form>
            </div>
        </AuthenticatedLayout>
    );
}
