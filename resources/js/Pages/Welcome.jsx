import { Link, Head, usePage } from "@inertiajs/react";
import AOS from "aos";
import { useEffect, useState } from "react";
import Header from "@/Components/Header";
import HeroHome from "@/Components/HeroHome";
import FeaturesHome from "@/Components/Features";
import FeaturesBlocks from "@/Components/FeaturesBlocks";
import Testimonials from "@/Components/Testimonials";
import Footer from "@/Components/Footer";
import Comments from "@/Components/Comments";
import { ContactForm } from "@/Components/ContactForm";
import SecondaryButton from "@/Components/SecondaryButton";
import Modal from "@/Components/Modal";

export default function Welcome(props) {
    const serverData = usePage().props;
    const services = serverData.services;
    const comments = serverData.comments;
    const message = serverData.message;
    const [showMessageModal, setShowMessageModal] = useState(false);

    useEffect(() => {
        setShowMessageModal(message !== null);
    }, [message]);

    useEffect(() => {
        AOS.init({
            once: true,
            disable: "phone",
            duration: 700,
            easing: "ease-out-cubic",
        });
    }, []);

    return (
        <>
            <Head title="Welcome" />
            <div className="flex flex-col min-h-screen overflow-hidden">
                <Header user={props.auth.user} />
                <main className="flex-grow">
                    <HeroHome />
                    <FeaturesHome services={services} />
                    <FeaturesBlocks />
                    <Testimonials />
                    <Comments comments={comments} />
                    <ContactForm />
                </main>
                <Footer />
                <Modal show={showMessageModal}>
                    <div className="p-6">
                        <h2 className="text-lg font-medium text-gray-900 mt-20">
                            Mensaje del sistema
                        </h2>
                        <p className="mt-1 text-sm text-gray-600">{message}</p>

                        <div className="mt-6 flex justify-end">
                            <SecondaryButton
                                onClick={() => setShowMessageModal(false)}
                            >
                                Aceptar
                            </SecondaryButton>
                        </div>
                    </div>
                </Modal>
            </div>
        </>
    );
}
