import { Link, Head, usePage } from "@inertiajs/react";
import AOS from "aos";
import { useEffect } from "react";
import Header from "@/Components/Header";
import HeroHome from "@/Components/HeroHome";
import FeaturesHome from "@/Components/Features";
import FeaturesBlocks from "@/Components/FeaturesBlocks";
import Testimonials from "@/Components/Testimonials";
import Newsletter from "@/Components/Newsletter";
import Footer from "@/Components/Footer";

export default function Welcome(props) {
    const serverData = usePage().props;
    const services = serverData.services;

    useEffect(() => {
        AOS.init({
            once: true,
            disable: "phone",
            duration: 700,
            easing: "ease-out-cubic",
        });
    }, []);

    return (
        <>
            <Head title="Services" />
            <div className="flex flex-col min-h-screen overflow-hidden">
                <Header user={props.auth.user} />
                <main className="flex-grow">
                    <FeaturesHome services={services} />
                    <FeaturesBlocks />
                </main>
                <Footer />
            </div>
        </>
    );
}
