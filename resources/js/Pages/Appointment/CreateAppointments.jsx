import moment from "moment";
import AuthenticatedLayout from "@/Layouts/AuthenticatedLayout";
import { Head, useForm, usePage } from "@inertiajs/react";
import { useEffect, useState } from "react";
import { Calendar } from "@/Components/Calendar";

const mapAppointments = (app = []) =>
    app.map((ap) => {
        const start = moment(ap.start_datetime);
        const end = moment(ap.end_datetime);
        return {
            id: ap.id,
            start_date: start.format("YYYY-MM-DD"),
            start_hour: start.format("HH:mm"),
            end_hour: end.format("HH:mm"),
            available: true,
        };
    });

export default function CreateAppointment(props) {
    const serverData = usePage().props;
    const isEdit = serverData.isEdit;
    const repair = serverData.repair || {};
    const repairId = Number(repair.id);
    const totalTime = Number(repair.total_time);
    const suggestedAppointments = serverData.appointments || [];
    const currentAppointments = serverData.current_appointments || [];
    const [currentTime, setCurrentTime] = useState(0);
    const [errors, setErrors] = useState({});
    const [appointments, setAppointments] = useState(
        mapAppointments(suggestedAppointments)
    );
    const { data, setData, post, put } = useForm({
        appointments: [],
        repairId,
    });

    const onChange = (index, e) => {
        const newAppointments = [...appointments];
        newAppointments[index] = {
            ...appointments[index],
            [e.target.name]: e.target.value,
        };
        setAppointments(newAppointments);
    };

    const validate = () => {
        const hasError = Object.keys(errors).find((key) =>
            Boolean(errors[key])
        );
        return !hasError && repairId;
    };

    const mapAppointmentsToSubmit = (app = []) =>
        app.map((ap) => {
            const start = moment(ap.start_date);
            const end = moment(ap.start_date);
            const [startH, startM] = splitHour(ap.start_hour);
            const [endH, endM] = splitHour(ap.end_hour);
            start.set("hours", startH);
            end.set("hours", endH);
            start.set("minutes", startM);
            end.set("minutes", endM);

            return {
                id: app.id,
                start_datetime: start.format("YYYY-MM-DD HH:mm:ss"),
                end_datetime: end.format("YYYY-MM-DD HH:mm:ss"),
            };
        });

    const onSubmit = (e) => {
        e.preventDefault();
        if (!validate()) {
            console.log("Invalid form");
            return;
        }
        if (!isEdit) {
            post(route("appointments.store"));
        } else {
            put(route("appointments.update_all"));
        }
    };

    useEffect(() => {
        const apps = mapAppointmentsToSubmit(appointments);
        const newData = {
            appointments: apps,
            repairId: repairId,
        };
        setData(newData);
    }, [appointments]);

    const splitHour = (hour = "") => {
        const [hours, minutes] = hour.split(":");
        return [
            Boolean(hours) ? Number(hours) : 0,
            Boolean(minutes) ? Number(minutes) : 0,
        ];
    };

    const getHours = (start_hour = "", end_hour = "") => {
        const [startH, startM] = splitHour(start_hour);
        const [endH, endM] = splitHour(end_hour);
        const start = moment();
        const end = moment();
        start.set("hours", startH);
        end.set("hours", endH);
        start.set("minutes", startM);
        end.set("minutes", endM);

        const hours = moment.duration(end.diff(start)).asHours();
        return hours;
    };

    const existsAppoitment = (
        start_date = "",
        start_hour = "",
        end_hour = "",
        currentAppointments = []
    ) => {
        let exists = false;
        let i = 0;
        const startA = getMomentObject(start_date, start_hour);
        const endA = getMomentObject(start_date, end_hour);

        while (!exists && i < currentAppointments.length) {
            const startB = moment(currentAppointments[i].start_datetime);
            const endB = moment(currentAppointments[i].end_datetime);
            exists = !(endA <= startB || startA >= endB);
            i += 1;
        }
        return exists;
    };

    const existsInSelectedAppointments = (
        start_date = "",
        start_hour = "",
        end_hour = "",
        currentAppointments = [],
        ignoreIndex = null
    ) => {
        let exists = false;
        let i = 0;
        const startA = getMomentObject(start_date, start_hour);
        const endA = getMomentObject(start_date, end_hour);

        while (!exists && i < currentAppointments.length) {
            const startB = getMomentObject(
                currentAppointments[i].start_date,
                currentAppointments[i].start_hour
            );
            const endB = getMomentObject(
                currentAppointments[i].start_date,
                currentAppointments[i].end_hour
            );
            exists = !(endA <= startB || startA >= endB) && i !== ignoreIndex;
            i += 1;
        }
        return exists;
    };

    const getMomentObject = (date = "", hour = "") => {
        const datetime = moment(date);
        const [hours, minutes] = splitHour(hour);
        datetime.set("hours", hours);
        datetime.set("minutes", minutes);
        return datetime;
    };

    useEffect(() => {
        let currentTime = 0;
        const newErrors = {};
        appointments.forEach((ap, index) => {
            const duration = getHours(ap.start_hour, ap.end_hour);
            const e = existsAppoitment(
                ap.start_date,
                ap.start_hour,
                ap.end_hour,
                appointments,
                index
            );
            let error = "";
            if (duration < 0) {
                error = "La hora de inicio no puede ser mayor a la hora de fin";
            } else if (duration === 0) {
                error = "La hora de inicio y fin no pueden ser iguales";
            } else if (duration > 8) {
                error = "Las citas no pueden ser de más de 8 horas";
            } else if (duration !== 1) {
                error = "Las citas no pueden durar más de una hora";
            } else if (
                existsAppoitment(
                    ap.start_date,
                    ap.start_hour,
                    ap.end_hour,
                    currentAppointments
                )
            ) {
                error =
                    "Este horario ya está ocupado por otra cita, por favor elige otra hora o día";
            } else if (
                existsInSelectedAppointments(
                    ap.start_date,
                    ap.start_hour,
                    ap.end_hour,
                    appointments,
                    index
                )
            ) {
                error =
                    "Este horario ya está ocupado por una cita de esta reparación";
            } else {
                currentTime += duration;
            }
            newErrors[index] = error;
        });
        setErrors((errors) => ({
            ...errors,
            ...newErrors,
        }));
        setCurrentTime(Math.floor(currentTime));
    }, [appointments]);

    return (
        <AuthenticatedLayout
            auth={props.auth}
            errors={props.errors}
            header={
                <h2 className="font-semibold text-xl text-gray-800 leading-tight">
                    Agentar cita(s)
                </h2>
            }
        >
            <Head title="Cliente" />

            <div className="md:container md:mx-auto px-5 flex justify-center">
                <Calendar
                    appointments={appointments}
                    onChangeAppointment={onChange}
                    totalTime={totalTime}
                    currentTime={currentTime}
                    errors={errors}
                    onSave={onSubmit}
                />
            </div>
        </AuthenticatedLayout>
    );
}
