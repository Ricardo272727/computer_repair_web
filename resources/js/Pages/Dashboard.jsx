import Modal from "@/Components/Modal";
import DangerButton from "@/Components/DangerButton";
import SecondaryButton from "@/Components/SecondaryButton";
import SecondaryLink from "@/Components/SecondaryLink";
import { Pagination } from "@/Components/Pagination";
import { useCancelRepair } from "@/hooks/useCancelRepair";
import AuthenticatedLayout from "@/Layouts/AuthenticatedLayout";
import { Head, useForm, usePage } from "@inertiajs/react";
import _ from "lodash";
import moment from "moment";
import { useStatusDict } from "@/hooks/useStatusDict";
import { useFinishRepair } from "@/hooks/useFinishRepair";

export default function Dashboard(props) {
    const data = usePage().props;
    const params = new URLSearchParams(window.location.search);
    const items = data.repairs;
    const role = _.get(data, "auth.user.role", "").trim();
    const paginationLinks = data.pagination.links || [];
    const cancelRepair = useCancelRepair();
    const finishRepair = useFinishRepair();
    const statusDict = useStatusDict();

    const form = useForm({
        date: params.get("date")
            ? params.get("date")
            : moment().format("YYYY-MM-DD"),
    });

    const onClickSearch = () => {
        console.log(form.data);
        window.location.href = `${route("repairs.index")}?date=${
            form.data.date
        }`;
    };

    const onChangeDateFilter = (e) => {
        let newDate = e.target.value;
        form.setData({ date: moment(newDate).format("YYYY-MM-DD") });
    };

    return (
        <AuthenticatedLayout
            auth={props.auth}
            errors={props.errors}
            header={
                <h2 className="font-semibold text-xl text-gray-800 leading-tight">
                    {role === "admin"
                        ? "Reparaciones de " +
                          moment(form.data.date).format("DD-MM-YYYY")
                        : "Reparaciones"}
                </h2>
            }
        >
            <Head title="Cliente" />
            <div className="container flex justify-end py-5 px-2">
                {role === "admin" ? (
                    <div
                        className="relative mb-3"
                        data-te-datepicker-init
                        data-te-input-wrapper-init
                    >
                        <label
                            htmlFor="floatingInput"
                            className="pointer-events-none mr-3"
                        >
                            Selecciona una fecha:
                        </label>
                        <input
                            type="date"
                            placeholder="Selecciona una fecha"
                            className="border-none rounded mr-3"
                            onChange={onChangeDateFilter}
                            value={form.data.date}
                        />
                        <SecondaryButton
                            processing={false}
                            onClick={onClickSearch}
                        >
                            Buscar
                        </SecondaryButton>
                    </div>
                ) : (
                    <>
                        <a
                            href={route("repairs.create")}
                            className="bg-gray-800 hover:bg-gray-500 text-white font-bold py-2 px-4 rounded cursor-pointer"
                        >
                            Nueva reparación
                        </a>
                        <a
                            href={route("comments.create")}
                            className="bg-gray-800 hover:bg-gray-500 text-white font-bold py-2 px-4 rounded cursor-pointer ml-4"
                        >
                            Dejar un comentario 
                        </a>
                    </>
                )}
            </div>
            <div className="md:container md:mx-auto px-2">
                <div className="flex flex-col">
                    <div className="overflow-x-auto sm:mx-0.5 lg:mx-0.5">
                        <div className="py-2 inline-block min-w-full sm:px-6 lg:px-8">
                            <div className="overflow-hidden">
                                <table className="min-w-full">
                                    <thead className="bg-white border-b">
                                        <tr>
                                            <th
                                                scope="col"
                                                className="text-sm font-medium text-gray-900 px-6 py-4 text-left"
                                            >
                                                ID
                                            </th>
                                            <th
                                                scope="col"
                                                className="text-sm font-medium text-gray-900 px-6 py-4 text-left"
                                            >
                                                Precio
                                            </th>
                                            <th
                                                scope="col"
                                                className="text-sm font-medium text-gray-900 px-6 py-4 text-left"
                                            >
                                                Status
                                            </th>
                                            <th
                                                scope="col"
                                                className="text-sm font-medium text-gray-900 px-6 py-4 text-left"
                                            >
                                                Tiempo (horas)
                                            </th>
                                            <th
                                                scope="col"
                                                className="text-sm font-medium text-gray-900 px-6 py-4 text-left"
                                            >
                                                Descripción
                                            </th>
                                            <th
                                                scope="col"
                                                className="text-sm font-medium text-gray-900 px-6 py-4 text-left"
                                            >
                                                Opciones
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {items.map((item, index) => (
                                            <tr
                                                key={index}
                                                className="bg-gray-100 border-b"
                                            >
                                                <td className="px-6 py-4 whitespace-nowrap text-sm font-medium text-gray-900">
                                                    {item.id}
                                                </td>
                                                <td className="text-sm text-gray-900 font-light px-6 py-4 whitespace-nowrap">
                                                    {"$" +
                                                        item.total_cost +
                                                        " MXN."}
                                                </td>
                                                <td className="text-sm text-gray-900 font-light px-6 py-4 whitespace-nowrap">
                                                    {statusDict[item.status] ||
                                                        "Desconocido"}
                                                </td>
                                                <td className="text-sm text-gray-900 font-light px-6 py-4 whitespace-nowrap">
                                                    {item.total_time}
                                                </td>
                                                <td className="text-sm text-gray-900 font-light px-6 py-4 whitespace-nowrap">
                                                    {item.services}
                                                </td>
                                                <td className="text-sm text-gray-900 font-light px-6 py-4 whitespace-nowrap">
                                                    <SecondaryLink
                                                        href={route(
                                                            "repairs.edit",
                                                            item.id
                                                        )}
                                                        processing={
                                                            item.status !==
                                                            "pending"
                                                        }
                                                    >
                                                        Editar
                                                    </SecondaryLink>
                                                    <SecondaryLink
                                                        className="ml-3"
                                                        href={route(
                                                            "repairs.show",
                                                            item.id
                                                        )}
                                                    >
                                                        Ver reparación
                                                    </SecondaryLink>
                                                </td>
                                                <td className="text-sm text-gray-900 font-light px-6 py-4 whitespace-nowrap">
                                                    {role === "client" ? (
                                                        <DangerButton
                                                            processing={
                                                                item.status ===
                                                                    "cancelled" ||
                                                                item.status ===
                                                                    "finished"
                                                            }
                                                            onClick={() =>
                                                                cancelRepair.onClickCancel(
                                                                    item
                                                                )
                                                            }
                                                        >
                                                            Cancelar
                                                        </DangerButton>
                                                    ) : (
                                                        <SecondaryButton
                                                            processing={
                                                                item.status !==
                                                                "pending"
                                                            }
                                                            onClick={() =>
                                                                finishRepair.onClick(
                                                                    item
                                                                )
                                                            }
                                                        >
                                                            Finalizar
                                                        </SecondaryButton>
                                                    )}
                                                </td>
                                            </tr>
                                        ))}
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="flex justify-end md:container md:mx-auto px-10 pt-5 pb-10">
                    <Pagination links={paginationLinks} />
                </div>
            </div>
            <Modal show={cancelRepair.show}>
                <div className="p-6">
                    <h2 className="text-lg font-medium text-gray-900 mt-20">
                        ¿Estás segur@ que deseas cancelar la reparación con el
                        ID: {cancelRepair.repair.id}?
                    </h2>
                    <p className="mt-1 text-sm text-gray-600">
                        Una vez cancelada la reparación, se cancelarán todas tus
                        citas y los horarios seleccionados estarán disponibles
                        para otros usuarios
                    </p>

                    <div className="mt-6 flex justify-end">
                        <SecondaryButton onClick={cancelRepair.onCloseModal}>
                            Regresar
                        </SecondaryButton>

                        <DangerButton
                            className="ml-3"
                            processing={false}
                            onClick={cancelRepair.onAcceptCancel}
                        >
                            Cancelar reparación
                        </DangerButton>
                    </div>
                </div>
            </Modal>
            <Modal show={finishRepair.show}>
                <div className="p-6">
                    <h2 className="text-lg font-medium text-gray-900 mt-20">
                        ¿Estás segur@ que deseas finalizar la reparación con el
                        ID: {finishRepair.repair.id}?
                    </h2>
                    <p className="mt-1 text-sm text-gray-600">
                        Una vez terminada la reparación, no se podrán editar las
                        citas y solo podrás ver los detalles de la reparación
                    </p>

                    <div className="mt-6 flex justify-end">
                        <SecondaryButton onClick={finishRepair.onCloseModal}>
                            Regresar
                        </SecondaryButton>

                        <DangerButton
                            className="ml-3"
                            processing={false}
                            onClick={finishRepair.onAccept}
                        >
                            Finalizar
                        </DangerButton>
                    </div>
                </div>
            </Modal>
        </AuthenticatedLayout>
    );
}
