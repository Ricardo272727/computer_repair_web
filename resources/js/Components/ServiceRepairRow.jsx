export default function ServiceRepairRow({
    id = -1,
    cost = 0,
    time = 0,
    onChangeService = null,
    index = 0,
    availableServices = [],
    onDeleteService = null,
}) {
    return (
        <div className="flex flex-wrap -mx-1 md:mb-4 border-b-2 border-black-100 pb-3 mb-6">
            <div className="w-full md:w-1/2 px-3 mb-6 md:mb-0">
                <label
                    className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2"
                    htmlFor="grid-state"
                >
                    Servicio
                </label>
                <div className="relative">
                    <select
                        className="block appearance-none w-full bg-gray-200 border border-gray-200 text-gray-700 py-3 px-2 pr-8 rounded leading-tight focus:outline-none focus:bg-white focus:border-gray-500"
                        id="grid-state"
                        name="id"
                        onChange={(e) => onChangeService(index, e)}
                        value={id}
                    >
                        {availableServices.map((s, index) => (
                            <option key={index} value={s.id}>
                                {s.name}
                            </option>
                        ))}
                    </select>
                </div>
            </div>

            <div className="w-full md:w-1/6 px-3 mb-6 md:mb-0">
                <label
                    className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2"
                    htmlFor="grid-city"
                >
                    Costo
                </label>
                <input
                    className="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500"
                    id="grid-city"
                    type="text"
                    placeholder="Costo"
                    value={cost}
                    disabled
                />
            </div>

            <div className="w-full md:w-1/6 px-3 mb-6 md:mb-0">
                <label
                    className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2"
                    htmlFor="grid-city"
                >
                    Horas
                </label>
                <input
                    className="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500"
                    id="grid-city"
                    type="text"
                    placeholder="Tiempo"
                    value={time}
                    disabled
                />
            </div>
            <div className="w-full md:w-1/6 px-3 mb-6 md:mb-0 flex justify-start items-end">
                <button
                    className="block px-4 py-3 border-none text-gray-700 font-bold bg-red-400 text-uppercase rounded text-white"
                    htmlFor="grid-city"
                    type="button"
                    onClick={(e) => onDeleteService(index, e)}
                >
                    Eliminar
                </button>
            </div>
        </div>
    );
}
