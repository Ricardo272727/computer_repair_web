import { useForm } from "@inertiajs/react";
import { useState } from "react";
import { ErrorAlert } from "./ErrorAlert";

export const ContactForm = () => {
    const form = useForm({
        email: "",
        subject: "",
        message: "",
    });
    const [error, setError] = useState("");

    const onChange = (e) => {
        form.setData({
            ...form.data,
            [e.target.name]: e.target.value,
        });
    };

    const onSubmit = (e) => {
        e.preventDefault();
        let errors = {
            email: !form.data.email ? "Correo electrónico es requerido" : "",
            subject: !form.data.subject ? "Asunto es requerido" : "",
            message: !form.data.message ? "Mensaje es requerido" : "",
            invalidEmail: !/^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/.test(
                form.data.email
            )
                ? "Correo electrónico inválido"
                : "",
        };
        let err = Object.values(errors).find((e) => e.length > 0);
        setError(err ? err : "");
        if (!err) {
            console.log(form.data);
            form.post(route("contacts.send"));
        }
    };

    return (
        <section className="bg-white">
            <div className="py-8 lg:py-16 px-4 mx-auto max-w-screen-md">
                <h2 className="mb-4 text-4xl tracking-tight font-extrabold text-center text-gray-900">
                    Contáctanos
                </h2>
                <p className="mb-8 lg:mb-16 font-light text-center text-gray-500 sm:text-xl">
                    ¿Tienes dudas acerca de nuestros servicios?, ¿Necesitas más
                    detalles de nuestros servicios?, escribe tus dudas y te
                    contestaremos por correo electrónico
                </p>
                <form onSubmit={onSubmit} className="space-y-8">
                    <div>
                        <label
                            htmlFor="email"
                            className="block mb-2 text-sm font-medium text-gray-900"
                        >
                            Correo electrónico
                        </label>
                        <input
                            type="email"
                            id="email"
                            name="email"
                            onChange={onChange}
                            className="shadow-sm bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-primary-500 focus:border-primary-500 block w-full p-2.5"
                            placeholder="correo@gmail.com"
                            required=""
                        />
                    </div>
                    <div>
                        <label
                            htmlFor="subject"
                            className="block mb-2 text-sm font-medium text-gray-900"
                        >
                            Asunto
                        </label>
                        <input
                            type="text"
                            id="subject"
                            name="subject"
                            onChange={onChange}
                            className="block p-3 w-full text-sm text-gray-900 bg-gray-50 rounded-lg border border-gray-300 shadow-sm focus:ring-primary-500 focus:border-primary-500"
                            placeholder="Como te podemos ayudar"
                            required=""
                        />
                    </div>
                    <div className="sm:col-span-2">
                        <label
                            htmlFor="message"
                            className="block mb-2 text-sm font-medium text-gray-900"
                        >
                            Mensaje
                        </label>
                        <textarea
                            id="message"
                            name="message"
                            onChange={onChange}
                            rows={6}
                            className="block p-2.5 w-full text-sm text-gray-900 bg-gray-50 rounded-lg shadow-sm border border-gray-300 focus:ring-primary-500 focus:border-primary-500"
                            placeholder="Deja un mensaje..."
                            defaultValue={""}
                        />
                    </div>
                    {error && <ErrorAlert message={error} />}
                    <button
                        type="submit"
                        className="py-3 px-5 text-sm font-medium text-center text-white rounded-lg bg-black sm:w-fit hover:bg-primary-800 focus:ring-4 focus:outline-none focus:ring-primary-300"
                    >
                        Enviar
                    </button>
                </form>
            </div>
        </section>
    );
};
