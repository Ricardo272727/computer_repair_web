import "../../css/calendar.css";

export const Calendar = ({
    appointments = [],
    onChangeAppointment = (e) => console.log("Not implemented", e),
    onSave = (e) => console.log("Not implemented", e),
    totalTime = 0,
    currentTime = 0,
    errors = {},
}) => {
    return (
        <div className="calendar w-full px-2">
            <div className="w-full">
                <div className="w-full bg-gray-200 rounded-full h-2.5 dark:bg-gray-700 mt-10">
                    <div
                        className="bg-green-600 h-2.5 rounded-full"
                        style={{
                            width: `${
                                totalTime > 0
                                    ? Math.floor(
                                          (currentTime * 100) / totalTime
                                      )
                                    : 0
                            }%`,
                        }}
                    ></div>
                </div>
                <p className="text-gray-900 mt-5 text-center">
                    Tiempo por cubrir{" "}
                    {currentTime + " " + "/" + " " + totalTime} hrs.
                </p>
                <p className="text-gray-900 mt-5 text-center">
                    En caso de no tener disponible alguna fecha o hora
                    sugeridas, por favor seleccione una nueva fecha/hora
                </p>
            </div>

            {appointments.map((s, index) => (
                <div className="container w-full border-b-4 pb-10" key={index}>
                    <div
                        className="container w-full flex md:justify-around mt-10 flex-col justify-start md:flex-row "
                        key={index}
                    >
                        <div className="w-full md:w-1/4">
                            <div className="flex items-center justify-center px-10">
                                <div className="datepicker relative form-floating mb-3 xl:w-96 w-full">
                                    <input
                                        type="date"
                                        className="form-control block w-full px-3 py-1.5 text-base font-normal text-gray-700 bg-white bg-clip-padding border border-solid border-gray-300 rounded transition ease-in-out m-0 focus:text-gray-700 focus:bg-white focus:border-blue-600 focus:outline-none"
                                        placeholder="Select a date"
                                        name="start_date"
                                        value={s.start_date}
                                        onChange={(e) =>
                                            onChangeAppointment(index, e)
                                        }
                                    />
                                    <label
                                        htmlFor="floatingInput"
                                        className="text-gray-700"
                                    >
                                        Dia
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div className="w-full md:w-1/4">
                            <div className="flex items-center justify-center px-10">
                                <div className="datepicker relative form-floating mb-3 xl:w-96 w-full">
                                    <input
                                        type="time"
                                        className="form-control block w-full px-3 py-1.5 text-base font-normal text-gray-700 bg-white bg-clip-padding border border-solid border-gray-300 rounded transition ease-in-out m-0 focus:text-gray-700 focus:bg-white focus:border-blue-600 focus:outline-none"
                                        placeholder="Select an hour"
                                        name="start_hour"
                                        value={s.start_hour}
                                        onChange={(e) =>
                                            onChangeAppointment(index, e)
                                        }
                                    />
                                    <label
                                        htmlFor="floatingInput"
                                        className="text-gray-700"
                                    >
                                        Hora inicio
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div className="w-full md:w-1/4">
                            <div className="flex items-center justify-center px-10">
                                <div className="datepicker relative form-floating mb-3 xl:w-96 w-full">
                                    <input
                                        type="time"
                                        className="form-control block w-full px-3 py-1.5 text-base font-normal text-gray-700 bg-white bg-clip-padding border border-solid border-gray-300 rounded transition ease-in-out m-0 focus:text-gray-700 focus:bg-white focus:border-blue-600 focus:outline-none"
                                        placeholder="Select an hour"
                                        name="end_hour"
                                        value={s.end_hour}
                                        onChange={(e) =>
                                            onChangeAppointment(index, e)
                                        }
                                    />
                                    <label
                                        htmlFor="floatingInput"
                                        className="text-gray-700"
                                    >
                                        Hora fin
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div className="w-16 md:w-16 mx-auto">
                            {!errors[index] ? (
                                <div className="flex items-center p-3 rounded-full justify-center bg-green-300">
                                    <svg
                                        xmlns="http://www.w3.org/2000/svg"
                                        viewBox="0 0 512 512"
                                    >
                                        <path d="M470.6 105.4c12.5 12.5 12.5 32.8 0 45.3l-256 256c-12.5 12.5-32.8 12.5-45.3 0l-128-128c-12.5-12.5-12.5-32.8 0-45.3s32.8-12.5 45.3 0L192 338.7 425.4 105.4c12.5-12.5 32.8-12.5 45.3 0z" />
                                    </svg>
                                </div>
                            ) : (
                                <div className="flex items-center p-3 rounded-full justify-center bg-red-300">
                                    <svg
                                        xmlns="http://www.w3.org/2000/svg"
                                        viewBox="0 0 320 512"
                                        style={{ width: 40, height: 40 }}
                                    >
                                        <path d="M310.6 150.6c12.5-12.5 12.5-32.8 0-45.3s-32.8-12.5-45.3 0L160 210.7 54.6 105.4c-12.5-12.5-32.8-12.5-45.3 0s-12.5 32.8 0 45.3L114.7 256 9.4 361.4c-12.5 12.5-12.5 32.8 0 45.3s32.8 12.5 45.3 0L160 301.3 265.4 406.6c12.5 12.5 32.8 12.5 45.3 0s12.5-32.8 0-45.3L205.3 256 310.6 150.6z" />
                                    </svg>
                                </div>
                            )}
                        </div>
                    </div>
                    <div className="container w-full px-10">
                        <div className="text-red-500">{errors[index]}</div>
                    </div>
                </div>
            ))}
            <div className="container flex justify-end py-5 px-2">
                <button
                    type="button"
                    className="bg-gray-800 hover:bg-gray-500 text-white font-bold py-2 px-4 rounded cursor-pointer"
                    onClick={onSave}
                >
                    Guardar citas
                </button>
            </div>
        </div>
    );
};
