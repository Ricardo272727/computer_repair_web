export const Pagination = ({ links = [] }) => (
    <nav aria-label="Page navigation example">
        <ul className="inline-flex -space-x-px">
            {links.map((link, index) => (
                <li key={index}>
                    {link.label.includes("Previous") ? (
                        <a
                            href={link.url}
                            aria-disabled={!link.active}
                            className={`px-3 py-2 ml-0 leading-tight text-gray-500 
                        bg-white border border-gray-300 rounded-l-lg hover:bg-gray-100 
                        hover:text-gray-700 dark:bg-white-800 dark:border-gray-700 
                        dark:text-gray-400 dark:hover:bg-gray-700 dark:hover:text-white
                    `}
                        >
                            Anterior
                        </a>
                    ) : link.label.includes("Next") ? (
                        <a
                            href={link.url}
                            aria-disabled={!link.active}
                            className={`px-3 py-2 leading-tight text-gray-500 bg-white border border-gray-300 
                            rounded-r-lg hover:bg-gray-100 hover:text-gray-700 dark:bg-white-800 dark:border-gray-700 
                            dark:text-gray-400 dark:hover:bg-gray-700 dark:hover:text-white
                            `}
                        >
                            Siguiente
                        </a>
                    ) : (
                        <a
                            href={link.url}
                            className={`px-3 py-2 leading-tight text-gray-500 
                            bg-white border border-gray-300 hover:bg-gray-100 
                            hover:text-gray-700 dark:bg-white-800 dark:border-gray-700 
                            dark:text-gray-400 dark:hover:bg-gray-700 dark:hover:text-white
                            `}
                        >
                            {link.label}
                        </a>
                    )}
                </li>
            ))}
        </ul>
    </nav>
);
