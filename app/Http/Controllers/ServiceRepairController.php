<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreServiceRepairRequest;
use App\Http\Requests\UpdateServiceRepairRequest;
use App\Models\ServiceRepair;

class ServiceRepairController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreServiceRepairRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreServiceRepairRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ServiceRepair  $serviceRepair
     * @return \Illuminate\Http\Response
     */
    public function show(ServiceRepair $serviceRepair)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ServiceRepair  $serviceRepair
     * @return \Illuminate\Http\Response
     */
    public function edit(ServiceRepair $serviceRepair)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateServiceRepairRequest  $request
     * @param  \App\Models\ServiceRepair  $serviceRepair
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateServiceRepairRequest $request, ServiceRepair $serviceRepair)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ServiceRepair  $serviceRepair
     * @return \Illuminate\Http\Response
     */
    public function destroy(ServiceRepair $serviceRepair)
    {
        //
    }
}
