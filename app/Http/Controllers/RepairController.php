<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreRepairRequest;
use App\Http\Requests\UpdateRepairRequest;
use App\Models\Appointment;
use App\Models\Service;
use App\Models\ServiceRepair;
use Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use App\Models\Repair;
use Inertia\Inertia;
use Symfony\Component\HttpFoundation\Request;

class RepairController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     */
    public function index(Request $request)
    {
        $validated = $request->validate(array('date' => 'date'));
        $date = null;
        if(array_key_exists('date', $validated)){
          $date = $validated['date'];
        }

        $repairs = DB::table('repairs')
        ->join('service_repairs', 'repairs.id', '=', 'service_repairs.repair_id')
        ->join('services', 'service_repairs.service_id', '=', 'services.id')
        ->join('appointments', 'appointments.repair_id', '=', 'repairs.id')
        ->select(
          'repairs.id as id', 
          'repairs.total_cost as total_cost', 
          'repairs.total_time as total_time',
          'repairs.created_at as created_at',
          DB::raw('trim(repairs.status) as status'),
          DB::raw("string_agg(services.name, ', ') as services"),
        );
        
        if($date) {
          $repairs = $repairs->where(
            DB::raw('appointments.start_datetime::DATE'), '=', $date
          );
        } else {
          $repairs = $repairs->where(
            DB::raw('appointments.start_datetime::DATE'), '=',  DB::raw("NOW()::DATE")
          );
        }

        $repairs = $repairs->groupBy('repairs.id')
        ->orderByDesc('id')
        ->paginate(10);

        return Inertia::render('Dashboard', [
            'repairs' => $repairs->items(),
            'pagination' => $repairs,            
        ]);
    }

    public function repairs_by_client(string $id)
    {    
        $repairs = DB::table('repairs')
        ->join('service_repairs', 'repairs.id', '=', 'service_repairs.repair_id')
        ->join('services', 'service_repairs.service_id', '=', 'services.id')
        ->select(
          'repairs.id as id', 
          'repairs.total_cost as total_cost', 
          'repairs.total_time as total_time',
          'repairs.created_at as created_at',
          DB::raw('trim(repairs.status) as status'),
          DB::raw("string_agg(services.name, ',') as services")
        )
        ->where('repairs.client_id', $id) 
        ->groupBy('repairs.id')
        ->orderByDesc('id')
        ->paginate(10);        
        $repairs->withPath('/repairs/' . $id);

        return Inertia::render('Dashboard', [
            'repairs' => $repairs->items(),
            'pagination' => $repairs
        ]);
    }

    public function cancel_repair(string $id)
    {    
        $repair = Repair::query()->first('id', $id);
        if(!$repair) return redirect()->back()->with('deleted', false);
        $status = trim($repair->status);
        if($status == 'cancelled' || $status == 'finished'){
          return redirect()->back()->with('deleted', false);
        }
        Repair::query()->where('id', $id)->update([
          'status' => 'cancelled'
        ]);

        $deleted = Appointment::where('repair_id', '=', $id)->delete();
          
        return redirect()->back()->with('deleted', true);
    }

    public function finish_repair(string $id)
    {    
        $repair = Repair::query()->first('id', $id); 
        if(!$repair){
          return redirect()->back()->with('deleted', true);
        }
        $status = trim($repair->status); 
        if($status == 'cancelled' || $status == 'finished'){
          return redirect()->back()->with('deleted', true);
        }
        Repair::query()->where('id', $id)->update([
          'status' => 'finished'
        ]);
        return redirect()->back()->with('deleted', true);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $services = Service::all();
        return Inertia::render('Repair/CreateRepair', [
            'services' => $services
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreRepairRequest  $request
     */
    public function store(StoreRepairRequest $request)
    {
        $data = $request->validated();
        $repair = new Repair;
        $repair->client_id = Auth::user()->id;
        $repair->total_cost = $data['totalCost'];
        $repair->total_time = $data['totalTime'];
        $repair->status = 'pending';
        $repair->save();

        foreach ($data['services'] as $service) {
            $serv_rep = new ServiceRepair;
            $serv_rep->repair_id = $repair->id;
            $serv_rep->service_id = $service;
            $serv_rep->save();
        }

        return Redirect::route('appointments.create')->with(['repair_id' => $repair->id]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Repair  $repair
     * @return \Illuminate\Http\Response
     */
    public function show(Repair $repair)
    {
        $user = DB::table('users')->where('id', $repair->client_id)->first();
        $appointments = Appointment::where('repair_id', $repair->id)->get();
        $services = DB::table('service_repairs')
          ->join('services', 'service_repairs.service_id', '=', 'services.id')
          ->select(
            'services.id', 
            'services.name', 
            'services.duration', 
            'services.price', 
            'services.description', 
          )
          ->where('service_repairs.repair_id', $repair->id) 
          ->orderByDesc('service_repairs.id')
          ->get();

        return Inertia::render('Repair/ShowRepair', [
          'services' => $services,
          'repair' => $repair,
          'user' => [
            'id' => $user->id,
            'name' => $user->name,
            'email' => $user->email,
            'phone_number' => $user->phone_number,
            'street' => $user->street,
            'street_number' => $user->street_number,
            'municiple' => $user->municiple,
            'city' => $user->city,
            'postal_code' => $user->postal_code,
          ],
          'appointments' => $appointments,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Repair  $repair
     */
    public function edit(Repair $repair)
    {
        $services = Service::all();
        $current_services = DB::table('service_repairs')
            ->leftJoin('services', 'service_repairs.service_id', '=', 'services.id')
            ->select('services.id', 'services.name', 'services.price', 'services.duration')
            ->where('service_repairs.repair_id', $repair->id)
            ->get();

        return Inertia::render('Repair/CreateRepair', [
            'repair' => $repair,
            'current_repair_services' => $current_services,
            'services' => $services,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateRepairRequest  $request
     */
    public function update(UpdateRepairRequest $request, Repair $repair)
    {
        $data = $request->validated();
        $id = $repair->id;
        DB::table('service_repairs')
            ->where('repair_id', $id)
            ->delete();

        foreach ($data['services'] as $service) {
            $serv_rep = new ServiceRepair;
            $serv_rep->repair_id = $repair->id;
            $serv_rep->service_id = $service;
            $serv_rep->save();
        }

        $repair->update([
            'total_cost' => $data['totalCost'],
            'total_time' => $data['totalTime'],
        ]);
        return Redirect::route('appointments.edit_all', [
            'repair_id' => $repair->id,
        ]); 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Repair  $repair
     * @return \Illuminate\Http\Response
     */
    public function destroy(Repair $repair)
    {
        //
    }
}
