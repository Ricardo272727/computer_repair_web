<?php

namespace App\Http\Controllers;

use App\Models\Comment;
use App\Http\Requests\StoreCommentRequest;
use Illuminate\Support\Facades\Auth;
use Inertia\Inertia;

class CommentController extends Controller
{

    public function store(StoreCommentRequest $request)
    {
        $data = $request->validated();
        $comment = new Comment;
        $comment->client_id = Auth::user()->id;
        $comment->content = $data['content'];
        $comment->save();

        return redirect()->route('client-repairs.index', ['id' => $comment->client_id]);
    }    

    public function create()
    {
        return Inertia::render('Comment/CreateComment'); 
    }  
}
