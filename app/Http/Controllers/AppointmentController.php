<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreAppointmentRequest;
use App\Http\Requests\UpdateAppointmentRequest;
use App\Models\Appointment;
use App\Models\Repair;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Inertia\Inertia;
use Illuminate\Http\Request;

class AppointmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     */
    public function index()
    {

        date_default_timezone_set('America/Monterrey');
        $user = Auth::user();
        $client_id = $user->id;

        $pagination = DB::table('appointments')->paginate(10);
        $role = trim($user->role);
        if($role == 'client'){
          $pagination = DB::table('appointments')
            ->where('client_id', $client_id)
            ->where('start_datetime', '>=', date('Y-m-d'))
            ->orderBy('id', 'desc')
            ->paginate(10);
        }

        return Inertia::render('Appointment/ListAppointments', [
            'pagination' => $pagination,
            'start_datetime' => date('Y-m-d'),            
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     */
    public function create()
    {        
        $repair_id = session()->get('repair_id');
        if(!$repair_id){
          return Redirect::route('repairs.create');
        }
        $repair = DB::table('repairs')->where('id', '=', $repair_id)->first();
        $appointments = $this->suggestAppointments($repair->total_time);
        $current_appointments = DB::table('appointments')->get();

        return Inertia::render('Appointment/CreateAppointments', [
          'repair' => $repair,
          'appointments' => $appointments,
          'current_appointments' => $current_appointments,
        ]);
    }

    public function suggestAppointments($hours=0){
      date_default_timezone_set('America/Monterrey');
      $today = date('Y-m-d');
      $default_schedule = [9, 10, 11, 12, 13, 14, 15, 16, 17, 18];
      $current_hours = 0;
      $current_day = date('Y-m-d');
      $apps = DB::table('appointments')->where('start_datetime', '>=', $today)->get();
      $appointments = [];
      
      while($current_hours < $hours){
        $schedule = $default_schedule;
        if($current_day == $today){
          $schedule = [];
          $today_hour = intval(date('H')); 
          foreach($default_schedule as $item){
            if($item > $today_hour){
              array_push($schedule, $item);
            }
          }
        }
        foreach($schedule as $hour){ 
          $start_datetime = $current_day . ' ' .  ($hour < 10 ? '0' . $hour : $hour) . ':00:00';
          $end_datetime = $current_day . ' ' .  ($hour+1) . ':00:00';
          $available = true;
          foreach($apps as $app){
            $available = false;
            $startA = date($app->start_datetime);
            $endA = date($app->end_datetime);
            $startB = $start_datetime;
            $endB = $end_datetime;
            $available = ($endA <= $startB || $startA >= $endB);
            if(!$available){
              break;
            }
          }
          if($available && $current_hours < $hours) {
            array_push($appointments, [
              'start_datetime' => $start_datetime, 
              'end_datetime' => $end_datetime,
            ]);
            $current_hours += 1;
          } 
        }
        $current_day = date('Y-m-d', strtotime($current_day. ' +1 day'));
      }

      return $appointments;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreAppointmentRequest  $request
     */
   public function store(StoreAppointmentRequest $request)      
    {
      $data = $request->validated();
      foreach($data['appointments'] as $app){
        $appointment = new Appointment;
        $appointment->start_datetime = $app['start_datetime'];
        $appointment->end_datetime = $app['end_datetime'];
        $appointment->status = 'pending';
        $appointment->repair_id = $data['repairId'];
        $appointment->client_id = Auth::user()->id;
        $appointment->save();
      }
      return Redirect::route('appointments.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Appointment  $appointment
     * @return \Illuminate\Http\Response
     */
    public function show(Appointment $appointment)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     */
    public function edit(Request $request)
    {
        $repair_id = $request->input('repair_id');
        $repair = Repair::query()->where('id', '=', $repair_id)->first();
        $appointments = $this->suggestAppointments($repair->total_time);
        $current_appointments = DB::table('appointments')->where('repair_id', '!=', $repair->id)->get();
        $appointments = DB::table('appointments')->where('repair_id', '=', $repair->id)->get();
        $time_diff = $this->getMissedAppointmentsTime($repair);
        if($time_diff > 0){
          $appointments = array_merge(
            $this->suggestAppointments($time_diff), 
            $appointments->toArray(),
          );
        } else if($time_diff < 0){
          $appointments = $this->removeLastAppointments($appointments, $time_diff * -1);
        }

        return Inertia::render('Appointment/CreateAppointments', [
          'repair' => $repair,
          'appointments' => $appointments,
          'current_appointments' => $current_appointments,
          'isEdit' => true, 
        ]);
    }

    public function getMissedAppointmentsTime(Repair $repair){
      $current_appointments = DB::table('appointments')
        ->where('repair_id', '=', $repair->id)
        ->get();
      $covered = count($current_appointments);
      return $repair->total_time - $covered;
    }

    public function removeLastAppointments($appointments, $last_count){
      $j = count($appointments) - 1;
      $removed_ids = [];
      for($i = 0; $i < $last_count; $i ++){
        array_push($removed_ids, $appointments[$j]->id);
        $j -= 1;
      }
      DB::table('appointments')->delete($removed_ids);
      $new_appointments = [];
      foreach($appointments as $app){
        $i = 0; 
        $found = false;
        while($i < count($removed_ids) && !$found){
          $found = $removed_ids[$i] == $app->id;
          $i++;
        }
        if(!$found){
          array_push($new_appointments, $app);
        }
      }
      return $new_appointments;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateAppointmentRequest  $request
     */
    public function update(UpdateAppointmentRequest $request)
    {
      $data = $request->validated();
      $updated_appointments = [];
      foreach($data['appointments'] as $app){
        if(!array_key_exists('id', $app)){
          $appointment = new Appointment;
          $appointment->start_datetime = $app['start_datetime'];
          $appointment->end_datetime = $app['end_datetime'];
          $appointment->status = 'pending';
          $appointment->repair_id = $data['repairId'];
          $appointment->client_id = Auth::user()->id;
          $appointment->save();
          array_push($updated_appointments, $appointment->id);
        } else {
          $appointment = Appointment::query()->find($app['id']);
          $appointment->update([
            'start_datetime' => $app['start_datetime'],
            'end_datetime' => $app['end_datetime'],
          ]);
          array_push($updated_appointments, $appointment->id);
        }
      }
      DB::table('appointments')
        ->where('repair_id', '=', $data['repairId'])
        ->whereNotIn('id', $updated_appointments)
        ->delete();
      return Redirect::route('appointments.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Appointment  $appointment
     * @return \Illuminate\Http\Response
     */
    public function destroy(Appointment $appointment)
    {
        //
    }
}
