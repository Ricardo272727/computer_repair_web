<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\SendContactRequest;
use App\Models\Contact;
use Illuminate\Support\Facades\DB;
use Inertia\Inertia;

class ContactController extends Controller
{
    //
    public function send(SendContactRequest $request) {
      $data = $request->validated();
      $contact = new Contact;
      $contact->email = $data['email'];
      $contact->message = $data['message'];
      $contact->subject = $data['subject'];
      $contact->save();

      return redirect()->route('welcome')->with('message', 'Se ha enviado tu mensaje, te enviaremos un correo electrónico lo más pronto posible');
    }

    public function index(){
      $contacts = DB::table('contacts')->orderBy('id', 'desc')->paginate(10);

      return Inertia::render('Contact/ListContacts', [
        'items' => $contacts->items(),
        'pagination' => $contacts
      ]);
    }
}
