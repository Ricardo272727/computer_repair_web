<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Appointment extends Model
{
    use HasFactory;
    protected $fillable = [
        'start_datetime',
        'end_datetime',
        'status',
        'repair_id',
        'client_id',
    ];

    protected $casts = [
        'created_at' => 'datetime',
        'start_datetime' => 'datetime',
        'end_datetime' => 'datetime',
    ];
}
