<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Repair extends Model
{
    use HasFactory;
    protected $fillable = [
        'client_id',
        'total_cost',
        'total_time',
        'status',
    ];

    protected $casts = [
        'created_at' => 'datetime',
    ];
}
