<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ServiceRepair extends Model
{
    use HasFactory;

    protected $fillable = [
        'repair_id',
        'service_id',
    ];
   
    protected $casts = [
        'created_at' => 'datetime',
    ];
}
