<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ServiceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {      
        DB::table('services')->insert([
            'name' => 'Limpieza de hardware',
            'description' => 'Desarmamos tu computadora y eliminamos todos los rastros de polvo y residuos acumulados por el uso diario.',
            'image' => 'https://cdn.pixabay.com/photo/2017/11/27/21/31/computer-2982270_960_720.jpg',
            'price' => 250,
            'duration' => 2,
            'deleted_at' => null
        ]);
        DB::table('services')->insert([
            'name' => 'Reparaciones de hardware',
            'description' => 'Reemplazamos componentes viejos de tu computadora y hacemos composturas a tarjetas de video o audio.',
            'image' => 'https://cdn.pixabay.com/photo/2014/08/26/21/27/service-428539_960_720.jpg',
            'price' => 350,
            'duration' => 3,
            'deleted_at' => null
        ]);
        DB::table('services')->insert([
            'name' => 'Armado de computadoras',
            'description' => 'Realizamos el armado de tu computadora nueva con los componentes seleccionados por ti.',
            'image' => 'https://cdn.pixabay.com/photo/2012/03/04/00/50/board-22098_960_720.jpg',
            'price' => 150,
            'duration' => 3,
            'deleted_at' => null
        ]);
        DB::table('services')->insert([
            'name' => 'Instalación de Windows 11',
            'description' => 'Instalamos o reinstalamos Windows 11, conservando tu información',
            'image' => 'https://cdn.pixabay.com/photo/2020/09/26/11/36/laptop-5603790_960_720.jpg',
            'price' => 240,
            'duration' => 2,
            'deleted_at' => null
        ]);
        DB::table('services')->insert([
            'name' => 'Instalación de drivers',
            'description' => 'Instalamos drivers de tarjetas gráficas y cualquier otro periférico',
            'image' => 'https://cdn.pixabay.com/photo/2015/09/05/20/02/coding-924920_960_720.jpg',
            'price' => 240,
            'duration' => 3,
            'deleted_at' => null
        ]);
    }
}
